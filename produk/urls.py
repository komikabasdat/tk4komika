from django.urls import path
from .views import viewKomik
from .views import viewMenu


from . import views

urlpatterns = [
    path('komik/', viewKomik, name='viewKomik'),
    path('menu/', viewMenu, name='viewMenu'),
]