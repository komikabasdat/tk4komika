from django.urls import re_path
from .views import login, logout
# from django.contrib.auth import views as auth_views

urlpatterns = [
    re_path(r'^login', login, name='login'),
     re_path(r'^logout', logout, name='login'),
    # re_path(r'^login', auth_views.login, {'template_name': 'login/index.html'}),
]
