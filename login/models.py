from django.db import models

# Create your models here.
class anggota (models.Model) :
    no_anggota = models.CharField(max_length=10)
    nama = models.CharField(max_length=255)
    alamat = models.TextField()
    tanggal_lahir = models.DateField()
    no_hp = models.CharField(max_length=15)
    ang_pembaca = models.CharField(max_length=255)
    ang_peminjam = models.CharField(max_length=255)
    password = models.CharField(max_length=255)
    is_anonymous =''
    USERNAME_FIELD = 'no_anggota'
    REQUIRED_FIELDS = ('no_anggota', 'password',)
    is_authenticated = ''

    # def set_is_authenticated(self, inp_is_auth) :
    #     is_authenticated = inp_is_auth