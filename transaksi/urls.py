from django.urls import path
from .views import viewTransaksiBaca
from .views import viewTransaksiPinjam

from . import views

urlpatterns = [
    path('baca', viewTransaksiBaca, name='viewTransaksiBaca'),
    path('pinjam', viewTransaksiPinjam, name='viewTransaksiPinjam'),
]