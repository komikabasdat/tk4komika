"""komika URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
# from django.urls import re_path
# import login.urls as loginurls
# import homepage.urls as homeurls
from django.contrib import admin


from django.conf.urls import include
from django.contrib.auth import views as auth_views
from django.conf.urls import url
# from django.contrib.sites import admin

urlpatterns = [
    # path('admin/', admin.site.urls),
    # re_path(r'^', include (loginurls)),
    path('', include('homepage.urls')),
    path('', include ('login.urls')),
    # path('user/', include('django.contrib.auth.urls')),
    # path('register/', include('registrasi.urls')),
    path('viewAnggota/', include('anggota.urls')),
    path('viewProduk/', include('produk.urls')),
    path('viewProduk/', include('produk.urls')),
    path('viewPesanan/', include('pesanan.urls')),
    path('viewTransaksi/', include('transaksi.urls')),
    path('viewBank/', include('bank.urls')),
]

