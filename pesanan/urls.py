from django.urls import path
from .views import viewPesanan

from . import views

urlpatterns = [
    path('', viewPesanan, name='viewPesanan'),
]
